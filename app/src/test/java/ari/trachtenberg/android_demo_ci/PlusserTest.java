package ari.trachtenberg.android_demo_ci;

import static org.junit.Assert.*;

import org.junit.Test;

public class PlusserTest {

    @Test
    public void plus() {
        assertEquals(Plusser.plus(1,1),2);
    }
}